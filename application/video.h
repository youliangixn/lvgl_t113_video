#ifndef _VIDEO_H_
#define _VIDEO_H_

#ifdef __cplusplus
extern "C"
{
#endif

#ifdef __cplusplus
}
#endif

#include <thread>
#include <mutex>
#include <string>
#include <string.h>
#include "application/video_gui.h"
#include "application/MediaPlayer.h"

using namespace std;

class Video;

class Video
{
public:
private:
    thread *pthread; // 独立于UI的线程
    mutex *ui_mutex;
    bool thread_exit_flag; // 线程退出标志
public:
    Video(exit_cb_t exit_cb, mutex &UIMutex, char *file_name);
    ~Video();

    char *video_path; // 视频文件路径
    
    const bool getExitFlag() const { return thread_exit_flag; }
    mutex *getUIMutex(void) const { return ui_mutex; }
};

#endif